#include <mpi.h>
#include <dlfcn.h>
#include <iostream>
#include <conduit.h>
#include <conduit_node.h>
#include <conduit_blueprint.hpp>
#include <conduit_cpp_to_c.hpp>

struct AscentAPI
{
private:
  using Ascent  = void;
public:
  using TYPE1 = Ascent* (*)();                        // Create
  using TYPE2 = void    (*)(Ascent*, conduit_node*);  // Open, Publish, Execute, Info
  using TYPE3 = void    (*)(Ascent*);                 // Close, Destroy

  Ascent* (*create)   ();
  void    (*open)     (Ascent *, conduit_node *);
  void    (*publish)  (Ascent *, conduit_node *);
  void    (*execute)  (Ascent *, conduit_node *);
  void    (*info)     (Ascent *, conduit_node *);
  void    (*close)    (Ascent *);
  void    (*destroy)  (Ascent *);
};

int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
  std::cout << "[main]" << std::endl;
  auto handle = dlopen( "libascent_mpi.so", RTLD_LAZY | RTLD_GLOBAL );
  if(!handle)
  {
    std::cerr << "Cannot open library : " << dlerror() << std::endl;
    return 1;
  }

  AscentAPI impl;

  impl.create   = (AscentAPI::TYPE1) dlsym(handle, "ascent_create"   );
  impl.open     = (AscentAPI::TYPE2) dlsym(handle, "ascent_open"     );
  impl.publish  = (AscentAPI::TYPE2) dlsym(handle, "ascent_publish"  );
  impl.execute  = (AscentAPI::TYPE2) dlsym(handle, "ascent_execute"  );
  impl.close    = (AscentAPI::TYPE3) dlsym(handle, "ascent_close"    );
  impl.destroy  = (AscentAPI::TYPE3) dlsym(handle, "ascent_destroy"  );

  if(impl.create && impl.open && impl.publish && impl.execute && impl.close && impl.destroy)
  {
    std::cout << "Found all functions" << std::endl;
  }

  void* ascent = impl.create();

  conduit::Node options;
  options["mpi_comm"] = MPI_Comm_c2f(MPI_COMM_WORLD);
  options["runtime/type"] = "ascent";
  conduit_node* options_c = conduit::c_node(&options);
  impl.open(ascent, options_c);

  conduit::Node mesh;
  conduit::blueprint::mesh::examples::braid("hexs", 50, 50, 50, mesh);
  conduit_node* mesh_c = conduit::c_node(&mesh);
  impl.publish(ascent, mesh_c);

  conduit::Node actions;
  conduit::Node &add_act = actions.append();
  add_act["action"] = "add_scenes";
  conduit::Node & scenes = add_act["scenes"];
  scenes["s1/plots/p1/type"] = "pseudocolor";
  scenes["s1/plots/p1/field"] = "braid";
  scenes["s1/image_name"] = "out_first_light_render_3d";
  std::cout << actions.to_yaml() << std::endl;
  conduit_node* actions_c = conduit::c_node(&actions);
  impl.execute(ascent, actions_c);

  impl.close(ascent);
  impl.destroy(ascent);

  MPI_Finalize();

  return 0;
}
