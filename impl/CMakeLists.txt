cmake_minimum_required(VERSION 3.23)
# When implementing the Catalyst API, as against using
# it to invoke Catalyst, one must use the component
# ``SDK`` in ``find_package`` call. This ensures that all necessary
# libraries etc. are available.
set(catalyst_DIR "/home/local/KHQ/abhi.yenpure/repositories/ascent/catalyst/install/lib/cmake/catalyst-2.0")
set(Ascent_DIR "/home/local/KHQ/abhi.yenpure/repositories/ascent/install/ascent-develop/lib/cmake/ascent")

find_package(catalyst REQUIRED COMPONENTS SDK)
find_package(Ascent REQUIRED)
find_package(MPI REQUIRED)

set(BUILD_SHARED_LIBS ON)

catalyst_implementation(
  NAME    ascent
  TARGET  catalyst-ascent
  SOURCES AscentCatalyst.cxx
  CATALYST_TARGET catalyst::catalyst)

target_link_libraries(catalyst-ascent PRIVATE ascent::ascent_mpi)
